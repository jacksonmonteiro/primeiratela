class Usuario:

	def __init__(self, usuario, senha):
		self.__usuario = usuario 
		self.__senha = senha

	@property
	def usuario(self):
		return self.__usuario

	@usuario.setter
	def usuario(self, u):
		self.__usuario = u 
	
	@property
	def senha(self):
		return self.__senha
	
	@senha.setter
	def senha(self, s):
		self.__senha = s