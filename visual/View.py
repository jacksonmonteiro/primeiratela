from kivy.app import App 
from kivy.uix.label import Label
from kivy.uix.boxlayout import BoxLayout 

from modelo.Modelo import Usuario
from controle.Controle import Controle

class Screen(BoxLayout):
	def login(self, usuario, senha):
		user = Usuario(usuario, senha)

		controle = Controle()
		controle.verificar(user)

class View(App):
	def build(self):
		return Screen()

View().run()